package scraper

import (
	"gitlab.com/pulsarr/server/pkg/manga"
	"gitlab.com/pulsarr/server/pkg/source"
)

type scraper int

const (
	MangaReader scraper = iota
	// MangaHere
)

type Scraper interface {
	Source() *source.Source
	SetSource(*source.Source)

	AllSeries() ([]*manga.Series, error)
	AllChapters(*manga.Series) ([]*manga.Chapter, error)
	AllPages(*manga.Chapter) ([]*manga.Page, error)
}

func newScraper(t scraper) Scraper {
	switch t {
	case MangaReader:
		s, _ := newMangaReader()
		return s
	// case MangaHere:
	// 	s, _ := newMangaHere()
	// 	return s
	default:
		s, _ := newMangaReader()
		return s
	}
}

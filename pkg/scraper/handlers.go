package scraper

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
)

type (
	ResponseError struct {
		Error string `json:"error"`
	}

	chaptersRequestBody struct {
		Chapters []int `json:"chapters"`
	}
)

func (s *service) ScrapeAllSeriesHandler(c echo.Context) error {
	s.manager.ScrapeAllSeries()

	return c.NoContent(http.StatusCreated)
}

func (s *service) ScrapeAllChaptersHandler(c echo.Context) error {
	seriesID := c.Param("series_id")

	series, err := s.mr.Find(seriesID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, ResponseError{
			fmt.Sprintf("unable to find manga with id: %s", seriesID),
		})
	}

	s.manager.ScrapeAllChapters(series)

	return c.NoContent(http.StatusCreated)
}

func (s *service) ScrapeAllPagesHandler(c echo.Context) error {
	seriesID := c.Param("series_id")

	crb := new(chaptersRequestBody)

	if err := c.Bind(crb); err != nil {
		return c.JSON(http.StatusBadRequest, ResponseError{
			Error: "unable to bind chapters, make sure its an array of int",
		})
	}

	series, err := s.mr.Find(seriesID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, ResponseError{
			fmt.Sprintf("unable to find manga with id: %s", seriesID),
		})
	}

	s.manager.ScrapePages(series, crb.Chapters...)

	return c.NoContent(http.StatusCreated)
}

// +build integration

package scraper

import (
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/pulsarr/server/pkg/manga"

	"github.com/stretchr/testify/assert"
)

func TestAllSeries(t *testing.T) {
	s, err := newMangaReader()
	assert.Nil(t, err)
	series, err := s.AllSeries()
	assert.Nil(t, err)

	assert.Nil(t, err)
	assert.Greater(t, len(series), 0)

	t.Run("manga_reader_chapters", testAllChapters(series[0]))
}

func testAllChapters(series *manga.Series) func(*testing.T) {
	return func(t *testing.T) {
		s, err := newMangaReader()
		assert.Nil(t, err)

		chapters, err := s.AllChapters(series)
		assert.Nil(t, err)
		assert.Greater(t, len(chapters), 0)

		firstChapter := chapters[0]
		assert.NotNil(t, firstChapter.DateReleased)
		assert.NotNil(t, firstChapter.DateScraped)

		t.Run("manga_reader_pages", testAllPages(firstChapter))
	}
}

func testAllPages(chapter *manga.Chapter) func(*testing.T) {
	return func(t *testing.T) {
		s, err := newMangaReader()
		assert.Nil(t, err)

		pages, err := s.AllPages(chapter)
		assert.Nil(t, err)

		for i := 0; i < len(pages); i++ {
			if pages[i].Image != nil {
				os.Remove(pages[i].Image.Name())
			}
		}

		assert.Nil(t, err)
		assert.Greater(t, len(pages), 0)
	}
}

func TestDownload(t *testing.T) {
	f, err := downloadImage("https://i6.mangareader.net/ichiba-kurogane-wa-kasegitai/1/ichiba-kurogane-wa-kasegitai-10790971.jpg")
	defer os.Remove(f.Name())

	assert.Nil(t, err)

	fs, err := f.Stat()
	assert.Nil(t, err)

	assert.Greater(t, fs.Size(), int64(0))
	assert.Equal(t, filepath.Ext(f.Name()), ".jpg")
}

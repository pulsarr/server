package scraper

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/labstack/gommon/log"

	"gitlab.com/pulsarr/server/pkg/manga"
	"gitlab.com/pulsarr/server/pkg/source"
)

type (
	Option func(*options)

	options struct {
		jobsPerSource int
		downloadPath  string
	}

	Manager interface {
		ScrapeAllSeries()
		ScrapeAllChapters(series *manga.Series)
		ScrapePages(series *manga.Series, chapters ...int)
	}

	manager struct {
		sr       source.Repository
		mr       manga.Repository
		scrapers map[string]*managedScraper
	}

	managedScraper struct {
		mr          manga.Repository
		destination string

		scraper          Scraper
		updateImagePaths chan *manga.Series
		saveSeries       chan *manga.Series

		scrapeSeries  chan int
		scrapeChapter chan *manga.Series
		scrapePages   chan *managedChapter
	}

	managedChapter struct {
		series  *manga.Series
		chapter int
	}
)

func JobsPerSource(jobsPerSource int) Option {
	return func(o *options) {
		o.jobsPerSource = jobsPerSource
	}
}

func DownloadPath(path string) Option {
	return func(o *options) {
		o.downloadPath = path
	}
}

func newManager(scrapers []Scraper, sr source.Repository, mr manga.Repository, setters ...Option) Manager {
	args := &options{
		jobsPerSource: 5,
		downloadPath:  os.Getenv("PULSARR_DOWNLOADS"),
	}

	for _, setter := range setters {
		setter(args)
	}

	mappedScrapers := map[string]*managedScraper{}
	for _, s := range scrapers {
		mappedScrapers[s.Source().ID] = &managedScraper{
			mr:               mr,
			scraper:          s,
			destination:      args.downloadPath,
			updateImagePaths: make(chan *manga.Series),
			saveSeries:       make(chan *manga.Series),
			scrapeSeries:     make(chan int),
			scrapeChapter:    make(chan *manga.Series),
			scrapePages:      make(chan *managedChapter),
		}
	}

	m := &manager{
		sr:       sr,
		mr:       mr,
		scrapers: mappedScrapers,
	}

	// One non-scraping working and x number of scraping workers
	for _, ms := range mappedScrapers {
		go newSourceWorker(ms)

		for i := 0; i < args.jobsPerSource; i++ {
			go newScrapeWorker(ms)
		}
	}

	return m
}

func newSourceWorker(ms *managedScraper) {
	for {
		select {
		case s := <-ms.updateImagePaths:
			migrateTempPages(s, ms.destination)
			go func(series *manga.Series) {
				ms.saveSeries <- series
			}(s)
		case s := <-ms.saveSeries:
			_ = ms.mr.Save(s)
		}
	}
}

func newScrapeWorker(ms *managedScraper) {
	for {
		select {
		case <-ms.scrapeSeries:
			allSeries, err := ms.scraper.AllSeries()
			if err != nil {
				log.Errorf("scrapeSeries: %v\n", err)
			}
			for _, s := range allSeries {
				ms.saveSeries <- s
			}
		case s := <-ms.scrapeChapter:
			chapters, err := ms.scraper.AllChapters(s)
			if err != nil {
				log.Errorf("scrapeChapter: %v\n", err)
			}
			s.Chapters = chapters
			ms.saveSeries <- s
		case mc := <-ms.scrapePages:
			c := mc.series.Chapters[mc.chapter]
			if c.Pages == nil {
				log.Infof("scrapePages: starting chapter %d\n", mc.chapter)
				p, err := ms.scraper.AllPages(c)
				if err != nil {
					log.Errorf("scrapePages: %v\n", err)
				} else {
					c.Pages = p
					log.Infof("scrapePages: done chapter %d\n", mc.chapter)
				}
			}
			ms.updateImagePaths <- mc.series
		}
	}
}

// ScrapeAllSeries will scrape series meta data for all registered scrapers
func (m *manager) ScrapeAllSeries() {
	for _, scraper := range m.scrapers {
		go func(ms *managedScraper) {
			ms.scrapeSeries <- 1
		}(scraper)
	}
}

// ScrapeAllChapters will scape chapter meta data for a given series
// series must have valid source id
func (m manager) ScrapeAllChapters(series *manga.Series) {
	if m.scrapers[series.Source.ID] != nil {
		go func() {
			m.scrapers[series.Source.ID].scrapeChapter <- series
		}()
	}
}

// ScapePages will scrape pages for a set of chapters passed
func (m manager) ScrapePages(series *manga.Series, chapters ...int) {
	if m.scrapers[series.Source.ID] != nil {
		go func() {
			for _, c := range chapters {
				m.scrapers[series.Source.ID].scrapePages <- &managedChapter{
					series:  series,
					chapter: c,
				}
			}
		}()
	}
}

//TODO: configure desired source dir
func migrateTempPages(series *manga.Series, destination string) {
	if destination == "" {
		return
	}

	for i, c := range series.Chapters {
		chapterDirName := leftPadding(len(series.Chapters), i+1)
		for j, p := range c.Pages {
			if p.Image == nil {
				continue
			}

			pageFileName := fmt.Sprintf(
				"%s%s",
				leftPadding(len(c.Pages), j+1),
				filepath.Ext(p.Image.Name()),
			)

			imageDir := filepath.Join(destination, series.Source.Name, series.Title, chapterDirName)
			_ = os.MkdirAll(imageDir, os.ModePerm)

			imagePath := filepath.Join(imageDir, pageFileName)
			err := os.Rename(p.Image.Name(), imagePath)
			if err == nil {
				f, _ := os.Open(filepath.Clean(imagePath))
				p.Image = f
			}
		}
	}
}

// leftPadding will calculate width based on total
// 999, 1 results in 001
func leftPadding(total, value int) string {
	format := fmt.Sprintf("%s%d%s", "%0", len(fmt.Sprintf("%d", total)), "d")
	return fmt.Sprintf(format, value)
}

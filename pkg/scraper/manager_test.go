package scraper

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/pulsarr/server/pkg/storage/mock"

	"gitlab.com/pulsarr/server/pkg/manga"
)

func waitForSeries(mr manga.Repository, timeout time.Duration) ([]*manga.Series, error) {
	for start := time.Now(); time.Since(start) < timeout; {
		series, err := mr.All(nil)
		if err != nil {
			return nil, err
		}

		if len(series) > 0 {
			return series, err
		}
	}

	return nil, fmt.Errorf("timeout reached %d", timeout)
}

func waitForChapters(mr manga.Repository, series *manga.Series, timeout time.Duration) (bool, error) {
	for start := time.Now(); time.Since(start) < timeout; {
		s, err := mr.Find(series.ID)
		if err != nil {
			return false, err
		}

		if len(s.Chapters) > 0 {
			return true, err
		}
	}

	return false, fmt.Errorf("timeout reached %d", timeout)
}

func waitForPages(mr manga.Repository, series *manga.Series, chapter int, timeout time.Duration) (bool, error) {
	for start := time.Now(); time.Since(start) < timeout; {
		s, err := mr.Find(series.ID)
		if err != nil {
			return false, err
		}

		if len(s.Chapters) > 0 && len(series.Chapters[chapter].Pages) > 0 {
			return true, nil
		}
	}

	return false, fmt.Errorf("timeout reached %d", timeout)
}

func TestHappyPath(t *testing.T) {
	test := func(opts ...Option) func(*testing.T) {
		return func(*testing.T) {
			sr := mock.NewSourceRepository()
			mr := mock.NewMangaRepository()

			ms := newMockScraper()
			err := sr.Save(ms.Source())
			assert.Nil(t, err)

			m := newManager([]Scraper{ms}, sr, mr, opts...)

			m.ScrapeAllSeries()

			series, err := waitForSeries(mr, time.Second)
			assert.Nil(t, err)

			assert.Greater(t, len(series), 0)

			firstSeries := series[0]

			m.ScrapeAllChapters(firstSeries)

			chaptersFound, err := waitForChapters(mr, firstSeries, time.Second*5)
			assert.Nil(t, err)
			assert.True(t, chaptersFound)
			assert.Greater(t, len(firstSeries.Chapters), 0)

			firstChapter := firstSeries.Chapters[0]

			m.ScrapePages(firstSeries, 0)
			pagesFound, err := waitForPages(mr, firstSeries, 0, time.Second*5)
			assert.Nil(t, err)
			assert.True(t, pagesFound)
			assert.Greater(t, len(firstChapter.Pages), 0)
		}
	}

	t.Run("defaultAmountOfJobs", test())
	t.Run("singleWorker", test(JobsPerSource(1)))
}

func TestMigrateNoDestination(t *testing.T) {
	migrateTempPages(mock.NewSeries(), "")
}

func TestMigrateTempPages(t *testing.T) {
	series := mock.NewSeries()
	destination, err := ioutil.TempDir("", "destination")
	assert.Nil(t, err)

	err = os.MkdirAll(destination, os.ModePerm)
	assert.Nil(t, err)

	assert.Greater(t, len(series.Chapters), 0)
	chapter := series.Chapters[0]

	assert.Greater(t, len(chapter.Pages), 0)

	f, err := ioutil.TempFile("", "page")
	assert.Nil(t, err)
	err = f.Close()
	assert.Nil(t, err)

	originalLocation := f.Name()

	page := chapter.Pages[0]
	page.Image = f

	migrateTempPages(series, destination)

	assert.NotEqual(t, originalLocation, page.Image.Name())
}

func TestLeftPadding(t *testing.T) {
	test := func(total, value int, expected string) func(*testing.T) {
		return func(*testing.T) {
			result := leftPadding(total, value)

			assert.Equal(t, expected, result)
		}
	}

	t.Run("1's", test(1, 1, "1"))
	t.Run("10's", test(10, 1, "01"))
	t.Run("100's", test(100, 1, "001"))
	t.Run("1_000's", test(1_000, 1, "0001"))
	t.Run("10_000's", test(10_000, 1, "00001"))

	t.Run("9", test(9, 9, "9"))
	t.Run("99", test(99, 99, "99"))
	t.Run("999", test(999, 999, "999"))
}

package scraper

import (
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/gocolly/colly"
	"gitlab.com/pulsarr/server/pkg/manga"
	"gitlab.com/pulsarr/server/pkg/source"
)

const (
	seriesListURL = "https://www.mangareader.net/alphabetical"
)

type mangaReader struct {
	seriesList *url.URL
	source     *source.Source
}

func newMangaReader() (Scraper, error) {
	u, err := url.Parse(seriesListURL)
	if err != nil {
		return nil, err
	}

	return &mangaReader{
		seriesList: u,
		source: &source.Source{
			Name: "Manga Reader",
			URL:  u,
		},
	}, nil
}

func (mr *mangaReader) Source() *source.Source {
	return mr.source
}

func (mr *mangaReader) SetSource(s *source.Source) {
	mr.source = s
}

// AllSeries should scrape the names and urls of all available manga series
// Do all work needed to enable calling AllChapters
func (mr *mangaReader) AllSeries() ([]*manga.Series, error) {
	c := colly.NewCollector()

	var scrapedSeries []*manga.Series

	c.OnHTML(".series_col li", func(e *colly.HTMLElement) {
		pathHref := e.ChildAttr("a", "href")
		href := e.Request.AbsoluteURL(pathHref)
		u, _ := url.Parse(href)

		s := manga.Series{
			Source:    mr.source,
			Title:     e.ChildText("a"),
			URL:       u,
			Completed: e.ChildText(".mangacompleted") != "",
		}

		scrapedSeries = append(scrapedSeries, &s)
	})

	if err := c.Visit(mr.seriesList.String()); err != nil {
		return nil, err
	}

	return scrapedSeries, nil
}

// AllChapters should get titles, and urls
// Do all work needed to enable calling AllPages
func (mr *mangaReader) AllChapters(s *manga.Series) ([]*manga.Chapter, error) {
	c := colly.NewCollector()

	var scrapedChapters []*manga.Chapter

	c.OnHTML("#listing tr:not(:first-child)", func(e *colly.HTMLElement) {
		pathHref := e.ChildAttr("a", "href")
		u, _ := url.Parse(e.Request.AbsoluteURL(pathHref))

		c := manga.Chapter{
			URL:   u,
			Title: e.ChildText("a"),
		}

		dateReleasedFormat := "01/02/2006"
		dateReleased, err := time.Parse(dateReleasedFormat, e.ChildText("td:not(:first-child)"))
		if err == nil {
			c.DateReleased = dateReleased
		}

		scrapedChapters = append(scrapedChapters, &c)
	})

	if err := c.Visit(s.URL.String()); err != nil {
		return nil, err
	}

	return scrapedChapters, nil
}

// AllPages scrape all images and save them to temporary file
func (mr *mangaReader) AllPages(chapter *manga.Chapter) ([]*manga.Page, error) {
	c := colly.NewCollector()

	// Do we want partial pages?
	var scrapedPages []*manga.Page

	c.OnHTML("#pageMenu option", func(e *colly.HTMLElement) {
		pathHref := e.Attr("value")
		sourcePageURL, _ := url.Parse(e.Request.AbsoluteURL(pathHref))

		// we lose status by doing this
		u, err := scrapeImageURL(sourcePageURL.String())
		if err == nil {
			f, downloadErr := downloadImage(u.String())

			if downloadErr == nil {
				p := manga.Page{
					URL:   u,
					Image: f,
				}

				scrapedPages = append(scrapedPages, &p)
			}
		}
	})

	if err := c.Visit(chapter.URL.String()); err != nil {
		return nil, err
	}

	return scrapedPages, nil
}

func scrapeImageURL(sourcePageURL string) (*url.URL, error) {
	c := colly.NewCollector()

	var u *url.URL
	var imageError error

	c.OnHTML("#img", func(e *colly.HTMLElement) {
		imageURL := e.Attr("src")
		u, imageError = url.Parse(imageURL)
	})

	if err := c.Visit(sourcePageURL); err != nil {
		return nil, err
	}

	return u, imageError
}

func downloadImage(imageURL string) (*os.File, error) {
	c := colly.NewCollector()

	var saveError error
	var tmpFile *os.File

	c.OnResponse(func(r *colly.Response) {
		if strings.Contains(r.Headers.Get("Content-Type"), "image") {
			imageFileExt := filepath.Ext(r.FileName())
			imagePattern := fmt.Sprintf("%s%s", "*", imageFileExt)
			f, err := ioutil.TempFile("", imagePattern)

			if err != nil {
				saveError = err
			} else {
				tmpFile = f
				saveError = r.Save(f.Name())
			}
		}
	})

	if err := c.Visit(imageURL); err != nil {
		return nil, err
	}

	return tmpFile, saveError
}

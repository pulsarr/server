package scraper

import (
	"net/url"

	"gitlab.com/pulsarr/server/pkg/manga"
	"gitlab.com/pulsarr/server/pkg/source"
	"gitlab.com/pulsarr/server/pkg/storage/mock"
)

type mockScraper struct {
	source *source.Source
}

func newMockScraper() Scraper {
	u, _ := url.Parse("https://example.com")

	return &mockScraper{
		source: &source.Source{
			Name: "MockScraper",
			URL:  u,
		}}
}

func (ms *mockScraper) Source() *source.Source {
	return ms.source
}

func (ms *mockScraper) SetSource(s *source.Source) {
	ms.source = s
}

func (ms *mockScraper) AllSeries() ([]*manga.Series, error) {
	series := []*manga.Series{
		mock.NewSeries(),
		mock.NewSeries(),
		mock.NewSeries(),
	}

	// Chapters need to be scraped separately
	for _, s := range series {
		s.Chapters = nil
		s.Source = ms.source
	}

	return series, nil
}

func (ms *mockScraper) AllChapters(*manga.Series) ([]*manga.Chapter, error) {
	chapters := []*manga.Chapter{
		mock.NewChapter(),
		mock.NewChapter(),
		mock.NewChapter(),
		mock.NewChapter(),
		mock.NewChapter(),
	}

	for _, c := range chapters {
		c.Pages = nil
	}

	return chapters, nil
}

func (ms *mockScraper) AllPages(chapter *manga.Chapter) ([]*manga.Page, error) {
	return []*manga.Page{
		mock.NewPage(),
		mock.NewPage(),
		mock.NewPage(),
		mock.NewPage(),
		mock.NewPage(),
		mock.NewPage(),
		mock.NewPage(),
		mock.NewPage(),
		mock.NewPage(),
		mock.NewPage(),
	}, nil
}

package scraper

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/pulsarr/server/pkg/manga"
	"gitlab.com/pulsarr/server/pkg/source"
)

type (
	service struct {
		manager Manager
		sr      source.Repository
		mr      manga.Repository
	}

	Service interface {
		ScrapeAllSeriesHandler(echo.Context) error
		ScrapeAllChaptersHandler(echo.Context) error
		ScrapeAllPagesHandler(echo.Context) error
	}
)

func New(sr source.Repository, mr manga.Repository, scrapers ...scraper) Service {
	var allScrapers []Scraper
	for _, s := range scrapers {
		convertedScraper := newScraper(s)
		allScrapers = append(allScrapers, convertedScraper)

		foundSource, err := sr.FindByName(convertedScraper.Source().Name)
		if err != nil {
			_ = sr.Save(convertedScraper.Source())
		} else {
			convertedScraper.SetSource(foundSource)
		}
	}

	return &service{
		manager: newManager(allScrapers, sr, mr),
		sr:      sr,
		mr:      mr,
	}
}

func (s *service) Manager() Manager {
	return s.manager
}

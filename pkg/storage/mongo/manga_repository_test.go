// +build integration

package mongo_test

import (
	"fmt"
	"net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/pulsarr/server/pkg/manga"
	"gitlab.com/pulsarr/server/pkg/source"
	"gitlab.com/pulsarr/server/pkg/storage/mock"
	"gitlab.com/pulsarr/server/pkg/storage/mongo"
)

func TestSeriesInsertAndUpdate(t *testing.T) {
	storage, err := mongo.New()
	assert.Nil(t, err)

	mr := storage.NewMangaRepository()

	series := mock.NewSeries()
	err = mr.Save(series)
	assert.Nil(t, err)
	assert.NotEqual(t, series.ID, "")

	foundSeries, err := mr.Find(series.ID)
	assert.Nil(t, err)

	compareSeries(t, series, foundSeries)

	u2 := &url.URL{
		Scheme: "http",
		Host:   "www.example2.com",
	}

	p2 := &manga.Page{
		URL:         u2,
		DateScraped: time.Now(),
	}

	c2 := &manga.Chapter{
		URL:          u2,
		Pages:        []*manga.Page{p2},
		Title:        fmt.Sprintf("%s%s", "Title2", time.Now().Format(time.RFC3339Nano)),
		DateReleased: time.Now(),
		DateScraped:  time.Now(),
	}

	updatedSeries := &manga.Series{
		ID:    series.ID,
		Title: fmt.Sprintf("%s%s", "Title2", time.Now().Format(time.RFC3339Nano)),
		URL:   u2,
		Source: &source.Source{
			Name: fmt.Sprintf("%s%s", "Source2", time.Now().Format(time.RFC3339Nano)),
			URL:  u2,
		},
		Chapters:     []*manga.Chapter{c2},
		Completed:    true,
		DateReleased: time.Now(),
		DateScraped:  time.Now(),
		Description:  fmt.Sprintf("%s%s", "description2", time.Now().Format(time.RFC3339Nano)),
		Artists:      []string{"Artist2"},
		Authors:      []string{"Artist2"},
	}

	err = mr.Save(updatedSeries)
	assert.Nil(t, err)

	foundUpdatedSeries, err := mr.Find(series.ID)
	assert.Nil(t, err)

	compareSeries(t, updatedSeries, foundUpdatedSeries)
}

func TestAll(t *testing.T) {
	storage, err := mongo.New()
	assert.Nil(t, err)
	mr := storage.NewMangaRepository()

	series := mock.NewSeries()
	err = mr.Save(series)
	assert.Nil(t, err)

	allSeries, err := mr.All(nil)
	assert.Nil(t, err)
	assert.Greater(t, len(allSeries), 0)

	found := false

	for i := 0; i < len(allSeries); i++ {
		if allSeries[i].ID == series.ID {
			found = true

			compareSeries(t, series, allSeries[i])
		}
	}

	assert.True(t, found)
}

func TestSeriesFindByTitle(t *testing.T) {
	storage, err := mongo.New()
	assert.Nil(t, err)
	mr := storage.NewMangaRepository()

	series := mock.NewSeries()
	err = mr.Save(series)
	assert.Nil(t, err)

	foundSeries, err := mr.FindByTitle(series.Title)
	assert.Nil(t, err)
	assert.NotNil(t, foundSeries)
	assert.Equal(t, len(foundSeries), 1)

	firstResult := foundSeries[0]

	compareSeries(t, series, firstResult)
}

func TestSeriesFind(t *testing.T) {
	storage, err := mongo.New()
	assert.Nil(t, err)
	mr := storage.NewMangaRepository()

	series := mock.NewSeries()
	err = mr.Save(series)
	assert.Nil(t, err)

	foundSeries, err := mr.Find(series.ID)
	assert.Nil(t, err)
	assert.NotNil(t, foundSeries)

	compareSeries(t, series, foundSeries)
}

func TestSeriesFindBySourceID(t *testing.T) {
	storage, err := mongo.New()
	assert.Nil(t, err)
	mr := storage.NewMangaRepository()

	series := mock.NewSeries()
	err = mr.Save(series)
	assert.Nil(t, err)

	foundSeries, err := mr.FindBySource(series.Source.ID)
	assert.Nil(t, err)
	assert.NotNil(t, foundSeries)
	assert.Greater(t, len(foundSeries), 0)
}

func compareSeries(t *testing.T, s1, s2 *manga.Series) {
	assert.Equal(t, s1.ID, s2.ID)
	assert.Equal(t, s1.Title, s2.Title)
	assert.Equal(t, s1.URL, s2.URL)
	assert.Equal(t, s1.Completed, s2.Completed)
	assert.Equal(t, s1.DateReleased.Format(time.RFC3339), s2.DateReleased.Format(time.RFC3339))
	assert.Equal(t, s1.DateScraped.Format(time.RFC3339), s2.DateScraped.Format(time.RFC3339))
	assert.Equal(t, s1.Description, s2.Description)
	assert.Equal(t, s1.Artists, s2.Artists)
	assert.Equal(t, s1.Authors, s2.Authors)

	assert.Equal(t, len(s1.Chapters), len(s2.Chapters))

	for i := 0; i < len(s1.Chapters); i++ {
		compareChapters(t, s1.Chapters[i], s2.Chapters[i])
	}
}

func compareChapters(t *testing.T, c1, c2 *manga.Chapter) {
	assert.Equal(t, c1.Title, c2.Title)
	assert.Equal(t, c1.URL, c2.URL)
	assert.Equal(t, c1.DateReleased.Format(time.RFC3339), c2.DateReleased.Format(time.RFC3339))
	assert.Equal(t, c1.DateScraped.Format(time.RFC3339), c2.DateScraped.Format(time.RFC3339))
	assert.Equal(t, len(c1.Pages), len(c2.Pages))

	for i := 0; i < len(c1.Pages); i++ {
		comparePages(t, c1.Pages[i], c2.Pages[i])
	}
}

func comparePages(t *testing.T, p1, p2 *manga.Page) {
	assert.Equal(t, p1.URL, p2.URL)
	assert.Equal(t, p1.DateScraped.Format(time.RFC3339), p2.DateScraped.Format(time.RFC3339))

	if p1.Image != nil && p2.Image != nil {
		assert.Equal(t, p1.Image.Name(), p2.Image.Name())
	} else {
		assert.Equal(t, p1.Image, p2.Image)
	}
}

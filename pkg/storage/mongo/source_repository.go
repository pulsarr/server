package mongo

import (
	"net/url"

	"gitlab.com/pulsarr/server/pkg/source"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type (
	sourceRepository struct {
		storage    storage
		collection *mongo.Collection
	}

	mongoSource struct {
		ID   primitive.ObjectID `bson:"_id,omitempty"`
		Name string             `bson:"name,omitempty"`
		URL  string             `bson:"url,omitempty"`
	}
)

func (s storage) NewSourceRepository() source.Repository {
	return &sourceRepository{
		storage:    s,
		collection: s.db.Collection("sources"),
	}
}

func convertSource(s *source.Source) *mongoSource {
	id, _ := primitive.ObjectIDFromHex(s.ID)
	return &mongoSource{
		ID:   id,
		Name: s.Name,
		URL:  s.URL.String(),
	}
}

func (ms *mongoSource) convert() *source.Source {
	u, _ := url.Parse(ms.URL)

	return &source.Source{
		ID:   ms.ID.Hex(),
		Name: ms.Name,
		URL:  u,
	}
}

// Save will insert if ID is empty and will update only non nil values
func (sr sourceRepository) Save(s *source.Source) error {
	ms := convertSource(s)

	if ms.ID.IsZero() {
		ms.ID = primitive.NewObjectID()
		if _, err := sr.collection.InsertOne(sr.storage.ctx, ms); err != nil {
			return err
		}
		s.ID = ms.ID.Hex()
	} else {
		filter := bson.D{primitive.E{
			Key:   "_id",
			Value: ms.ID,
		}}

		update := bson.D{
			primitive.E{
				Key:   "$set",
				Value: ms,
			},
		}

		if _, err := sr.collection.UpdateOne(sr.storage.ctx, filter, update); err != nil {
			return err
		}
	}

	return nil
}

func (sr sourceRepository) All() ([]*source.Source, error) {
	var allSources []*source.Source

	filter := bson.D{{}}

	cur, err := sr.collection.Find(sr.storage.ctx, filter)
	if err != nil {
		return nil, err
	}

	defer cur.Close(sr.storage.ctx)

	for cur.Next(sr.storage.ctx) {
		var ms mongoSource
		if err := cur.Decode(&ms); err != nil {
			return nil, err
		}

		s := ms.convert()

		allSources = append(allSources, s)
	}

	return allSources, nil
}

func (sr sourceRepository) Find(id string) (*source.Source, error) {
	mid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	filter := bson.D{
		primitive.E{
			Key:   "_id",
			Value: mid,
		},
	}

	ms := new(mongoSource)
	result := sr.collection.FindOne(sr.storage.ctx, filter)
	err = result.Decode(&ms)

	return ms.convert(), err
}

func (sr sourceRepository) FindByName(name string) (*source.Source, error) {
	filter := bson.D{
		primitive.E{
			Key:   "name",
			Value: name,
		},
	}

	ms := new(mongoSource)
	result := sr.collection.FindOne(sr.storage.ctx, filter)
	err := result.Decode(&ms)

	return ms.convert(), err
}

// +build integration

package mongo_test

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/pulsarr/server/pkg/storage/mock"
	"gitlab.com/pulsarr/server/pkg/storage/mongo"
)

func TestSourceInsertAndUpdate(t *testing.T) {
	storage, err := mongo.New()
	assert.Nil(t, err)
	sr := storage.NewSourceRepository()

	s := mock.NewSource()
	err = sr.Save(s)
	assert.Nil(t, err)
	assert.NotEqual(t, s.ID, "")

	foundSource, err := sr.Find(s.ID)
	assert.Nil(t, err)
	assert.NotEmpty(t, s.ID)
	assert.Equal(t, s.ID, foundSource.ID)
	assert.Equal(t, s.Name, foundSource.Name)
	assert.Equal(t, s.URL, foundSource.URL)

	updatedName := fmt.Sprintf("%s%s", "UpdatedName", time.Now().Format(time.RFC3339Nano))
	s.Name = updatedName

	updatedURL := mock.NewURL()
	s.URL = updatedURL

	err = sr.Save(s)
	assert.Nil(t, err)

	foundUpdatedSource, err := sr.Find(s.ID)
	assert.Nil(t, err)
	assert.Equal(t, s.ID, foundUpdatedSource.ID)
	assert.Equal(t, s.Name, foundUpdatedSource.Name)
	assert.Equal(t, s.URL, foundUpdatedSource.URL)
}

func TestSourceAll(t *testing.T) {
	storage, err := mongo.New()
	assert.Nil(t, err)
	sr := storage.NewSourceRepository()

	s := mock.NewSource()
	err = sr.Save(s)
	assert.Nil(t, err)

	allSeries, err := sr.All()
	assert.Nil(t, err)
	assert.Greater(t, len(allSeries), 0)

	found := false

	for i := 0; i < len(allSeries); i++ {
		if allSeries[i].ID == s.ID {
			found = true

			assert.Equal(t, s.Name, allSeries[i].Name)
			assert.Equal(t, s.URL, allSeries[i].URL)
		}
	}

	assert.True(t, found)
}

func TestSourceFindByName(t *testing.T) {
	storage, err := mongo.New()
	assert.Nil(t, err)
	sr := storage.NewSourceRepository()

	s := mock.NewSource()
	err = sr.Save(s)
	assert.Nil(t, err)

	foundSource, err := sr.FindByName(s.Name)
	assert.Nil(t, err)
	assert.NotNil(t, foundSource)

	assert.Equal(t, s.ID, foundSource.ID)
	assert.Equal(t, s.Name, foundSource.Name)
	assert.Equal(t, s.URL, foundSource.URL)
}

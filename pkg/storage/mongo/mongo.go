package mongo

import (
	"context"
	"os"

	"gitlab.com/pulsarr/server/pkg/manga"
	"gitlab.com/pulsarr/server/pkg/source"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	defaultMongoURL = "mongodb://localhost:27017"
	defaultDBName   = "pulsarr"
)

type (
	storage struct {
		db  *mongo.Database
		ctx context.Context
	}

	Storage interface {
		NewSourceRepository() source.Repository
		NewMangaRepository() manga.Repository
	}
)

func New() (Storage, error) {
	s := &storage{
		ctx: context.Background(),
	}

	ctx, cancel := context.WithCancel(s.ctx)
	defer cancel()

	mongoURL := os.Getenv("MONGO_URL")
	if mongoURL == "" {
		mongoURL = defaultMongoURL
	}

	client, err := mongo.NewClient(options.Client().ApplyURI(mongoURL))
	if err != nil {
		return nil, err
	}

	if err := client.Connect(ctx); err != nil {
		return nil, err
	}

	dbName := os.Getenv("MONGO_DB_NAME")
	if dbName == "" {
		dbName = defaultDBName
	}

	s.db = client.Database(dbName)

	return s, nil
}

package mongo_test

import (
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	_ = os.Setenv("MONGO_DB_NAME", "pulsarr-test")

	os.Exit(m.Run())
}

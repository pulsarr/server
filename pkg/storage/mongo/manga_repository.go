package mongo

import (
	"net/url"
	"os"
	"time"

	"gitlab.com/pulsarr/server/pkg/manga"
	"gitlab.com/pulsarr/server/pkg/source"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type (
	mangaRepository struct {
		storage          storage
		collection       *mongo.Collection
		sourceRepository source.Repository
	}

	mongoSeries struct {
		ID    primitive.ObjectID `bson:"_id,omitempty"`
		Title string             `bson:"title,omitempty"`
		URL   string             `bson:"url,omitempty"`

		SourceID primitive.ObjectID `bson:"source_id,omitempty"`
		Chapters []*mongoChapter    `bson:"chapters,omitempty"`

		Completed    bool      `bson:"completed"`
		DateReleased time.Time `bson:"date_released,omitempty"`
		DateScraped  time.Time `bson:"date_scraped,omitempty"`
		Description  string    `bson:"description,omitempty"`

		Artists []string `bson:"artists,omitempty"`
		Authors []string `bson:"authors,omitempty"`
	}

	mongoChapter struct {
		URL string `bson:"url,omitempty"`

		Pages []*mongoPage `bson:"pages,omitempty"`

		Title        string    `bson:"title,omitempty"`
		DateReleased time.Time `bson:"date_released,omitempty"`
		DateScraped  time.Time `bson:"date_scraped,omitempty"`
	}

	mongoPage struct {
		URL string `bson:"url,omitempty"`

		DateScraped time.Time `bson:"date_scraped,omitempty"`

		ImagePath string `bson:"image_path,omitempty"`
	}
)

const (
	PageSize = int64(10)
)

func (s storage) NewMangaRepository() manga.Repository {
	return &mangaRepository{
		storage:          s,
		collection:       s.db.Collection("series"),
		sourceRepository: s.NewSourceRepository(),
	}
}

func convertSeries(s *manga.Series) *mongoSeries {
	id, _ := primitive.ObjectIDFromHex(s.ID)

	var sid primitive.ObjectID
	if s.Source != nil {
		sid, _ = primitive.ObjectIDFromHex(s.Source.ID)
	}

	var convertedChapters []*mongoChapter

	for i := 0; i < len(s.Chapters); i++ {
		convertedChapters = append(convertedChapters, convertChapter(s.Chapters[i]))
	}

	return &mongoSeries{
		ID:           id,
		Title:        s.Title,
		URL:          s.URL.String(),
		SourceID:     sid,
		Chapters:     convertedChapters,
		Completed:    s.Completed,
		DateReleased: s.DateReleased,
		DateScraped:  s.DateScraped,
		Description:  s.Description,
		Artists:      s.Artists,
		Authors:      s.Authors,
	}
}

func convertChapter(c *manga.Chapter) *mongoChapter {
	var convertedPages []*mongoPage

	for i := 0; i < len(c.Pages); i++ {
		convertedPages = append(convertedPages, convertPage(c.Pages[i]))
	}

	return &mongoChapter{
		Title:        c.Title,
		URL:          c.URL.String(),
		DateReleased: c.DateReleased,
		DateScraped:  c.DateScraped,
		Pages:        convertedPages,
	}
}

func convertPage(p *manga.Page) *mongoPage {
	imagePath := ""
	if p.Image != nil {
		imagePath = p.Image.Name()
	}

	return &mongoPage{
		URL:         p.URL.String(),
		DateScraped: p.DateScraped,
		ImagePath:   imagePath,
	}
}

func (ms *mongoSeries) convert(sr source.Repository) *manga.Series {
	u, _ := url.Parse(ms.URL)

	var chapters []*manga.Chapter

	for i := 0; i < len(ms.Chapters); i++ {
		chapters = append(chapters, ms.Chapters[i].convert())
	}

	s, _ := sr.Find(ms.SourceID.Hex())

	loc := time.Now().Location()

	return &manga.Series{
		ID:           ms.ID.Hex(),
		Title:        ms.Title,
		URL:          u,
		Source:       s,
		Chapters:     chapters,
		Completed:    ms.Completed,
		DateReleased: ms.DateReleased.In(loc),
		DateScraped:  ms.DateScraped.In(loc),
		Description:  ms.Description,
		Artists:      ms.Artists,
		Authors:      ms.Authors,
	}
}

func (mc *mongoChapter) convert() *manga.Chapter {
	u, _ := url.Parse(mc.URL)

	var pages []*manga.Page

	for i := 0; i < len(mc.Pages); i++ {
		pages = append(pages, mc.Pages[i].convert())
	}

	loc := time.Now().Location()

	return &manga.Chapter{
		URL:          u,
		Pages:        pages,
		Title:        mc.Title,
		DateReleased: mc.DateReleased.In(loc),
		DateScraped:  mc.DateReleased.In(loc),
	}
}

func (mp *mongoPage) convert() *manga.Page {
	u, _ := url.Parse(mp.URL)
	f, _ := os.Open(mp.ImagePath)

	loc := time.Now().Location()

	return &manga.Page{
		URL:         u,
		DateScraped: mp.DateScraped.In(loc),
		Image:       f,
	}
}

// Save will insert if ID is empty and will update only non nil values
func (mr mangaRepository) Save(s *manga.Series) error {
	if err := mr.sourceRepository.Save(s.Source); err != nil {
		return err
	}

	ms := convertSeries(s)

	if ms.ID.IsZero() {
		ms.ID = primitive.NewObjectID()
		if _, err := mr.collection.InsertOne(mr.storage.ctx, ms); err != nil {
			return err
		}
		s.ID = ms.ID.Hex()
	} else {
		filter := bson.D{primitive.E{
			Key:   "_id",
			Value: ms.ID,
		}}

		update := bson.D{
			primitive.E{
				Key:   "$set",
				Value: ms,
			},
		}

		if _, err := mr.collection.UpdateOne(mr.storage.ctx, filter, update); err != nil {
			return err
		}
	}

	return nil
}

func (mr mangaRepository) All(allOpts *manga.AllOpts) ([]*manga.Series, error) {
	if allOpts == nil {
		thing := int64(10)
		allOpts = &manga.AllOpts{
			PageSize: &thing,
		}
	}

	var allSeries []*manga.Series

	filter := bson.D{{}}

	cur, err := mr.collection.Find(mr.storage.ctx, filter, &options.FindOptions{
		Limit: allOpts.PageSize,
	})
	if err != nil {
		return nil, err
	}

	defer cur.Close(mr.storage.ctx)

	for cur.Next(mr.storage.ctx) {
		var ms mongoSeries
		if err := cur.Decode(&ms); err != nil {
			return nil, err
		}

		s := ms.convert(mr.sourceRepository)

		allSeries = append(allSeries, s)
	}

	return allSeries, nil
}

func (mr mangaRepository) Find(id string) (*manga.Series, error) {
	mid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	filter := bson.D{
		primitive.E{
			Key:   "_id",
			Value: mid,
		},
	}

	ms := new(mongoSeries)
	result := mr.collection.FindOne(mr.storage.ctx, filter)
	err = result.Decode(&ms)

	return ms.convert(mr.sourceRepository), err
}

func (mr mangaRepository) FindByTitle(title string) ([]*manga.Series, error) {
	var allSeries []*manga.Series

	filter := bson.D{
		primitive.E{
			Key:   "title",
			Value: title,
		},
	}

	cur, err := mr.collection.Find(mr.storage.ctx, filter)
	if err != nil {
		return nil, err
	}

	defer cur.Close(mr.storage.ctx)

	for cur.Next(mr.storage.ctx) {
		var ms mongoSeries
		if err := cur.Decode(&ms); err != nil {
			return nil, err
		}

		s := ms.convert(mr.sourceRepository)

		allSeries = append(allSeries, s)
	}

	return allSeries, nil
}

func (mr mangaRepository) FindBySource(sourceID string) ([]*manga.Series, error) {
	sid, err := primitive.ObjectIDFromHex(sourceID)
	if err != nil {
		return nil, err
	}

	var allSeries []*manga.Series

	filter := bson.D{
		primitive.E{
			Key:   "source_id",
			Value: sid,
		},
	}

	cur, err := mr.collection.Find(mr.storage.ctx, filter)
	if err != nil {
		return nil, err
	}

	defer cur.Close(mr.storage.ctx)

	for cur.Next(mr.storage.ctx) {
		var ms mongoSeries
		if err := cur.Decode(&ms); err != nil {
			return nil, err
		}

		s := ms.convert(mr.sourceRepository)

		allSeries = append(allSeries, s)
	}

	return allSeries, nil
}

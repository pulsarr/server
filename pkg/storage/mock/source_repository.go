package mock

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/pulsarr/server/pkg/source"
)

type (
	sourceRepository struct{}
)

var (
	sources []*source.Source
)

func NewSourceRepository() source.Repository {
	return &sourceRepository{}
}

func (sr sourceRepository) Save(s *source.Source) error {
	if s.ID == "" {
		s.ID = fmt.Sprintf(
			"%s%d%d%d%d",
			time.Now().Format(time.RFC3339),
			rand.Intn(10),
			rand.Intn(10),
			rand.Intn(10),
			rand.Intn(10),
		)
		sources = append(sources, s)
		return nil
	}

	for i := 0; i < len(sources); i++ {
		if s.ID == sources[i].ID {
			sources[i] = s
			return nil
		}
	}

	return fmt.Errorf("%s%s", "Unfound source with ID", s.ID)
}

func (sr sourceRepository) All() ([]*source.Source, error) {
	return sources, nil
}

func (sr sourceRepository) Find(id string) (*source.Source, error) {
	for i := 0; i < len(sources); i++ {
		if id == sources[i].ID {
			return sources[i], nil
		}
	}

	return nil, fmt.Errorf("%s%s", "Unfound source with ID", id)
}

func (sr sourceRepository) FindByName(name string) (*source.Source, error) {
	for i := 0; i < len(sources); i++ {
		if name == sources[i].Name {
			return sources[i], nil
		}
	}

	return nil, fmt.Errorf("%s%s", "Unfound source with name", name)
}

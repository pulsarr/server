package mock_test

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/pulsarr/server/pkg/source"

	"gitlab.com/pulsarr/server/pkg/storage/mock"
)

// assertSameSource does not check ID
func assertSameSource(t *testing.T, expected, actual *source.Source) {
	assert.Equal(t, expected.ID, actual.ID)
	assert.Equal(t, expected.Name, actual.Name)
	assert.Equal(t, expected.URL, actual.URL)
	assert.Equal(t, expected.ID, actual.ID)
}

func TestSaveAndUpdateSource(t *testing.T) {
	sr := mock.NewSourceRepository()
	originalSource := mock.NewSource()
	err := sr.Save(originalSource)
	assert.Nil(t, err)

	sources, err := sr.All()
	assert.Nil(t, err)
	assert.Len(t, sources, 1)

	foundSource := sources[0]

	originalID := originalSource.ID
	assert.NotEqual(t, originalSource.ID, "")
	assertSameSource(t, originalSource, foundSource)

	u2, err := url.Parse("https://example2.com")
	assert.Nil(t, err)
	originalSource.Name = "Updated"
	originalSource.URL = u2

	err = sr.Save(originalSource)
	assert.Nil(t, err)

	sources, err = sr.All()
	assert.Nil(t, err)
	assert.Len(t, sources, 1)

	foundSource = sources[0]

	assert.Equal(t, originalID, foundSource.ID)
	assertSameSource(t, originalSource, foundSource)
}

func TestSourceSaveError(t *testing.T) {
	u, err := url.Parse("https://example.com")
	assert.Nil(t, err)

	sr := mock.NewSourceRepository()
	err = sr.Save(&source.Source{
		ID:   "idThatWontBeFound",
		Name: "mock",
		URL:  u,
	})

	assert.NotNil(t, err)
}

func TestSourceFindAndFindByName(t *testing.T) {
	sr := mock.NewSourceRepository()

	originalSource := mock.NewSource()
	err := sr.Save(originalSource)
	assert.Nil(t, err)

	sourceByName, err := sr.FindByName(originalSource.Name)
	assert.Nil(t, err)
	assert.NotNil(t, sourceByName)

	assert.NotEqual(t, originalSource.ID, "")
	assertSameSource(t, originalSource, sourceByName)

	sourceByID, err := sr.Find(originalSource.ID)
	assert.Nil(t, err)
	assert.NotNil(t, sourceByName)

	assert.NotEqual(t, originalSource.ID, "")
	assertSameSource(t, originalSource, sourceByID)
}

func TestSourceFindError(t *testing.T) {
	sr := mock.NewSourceRepository()
	_, err := sr.Find("NotARealID")
	assert.NotNil(t, err)
}

func TestSourceFindByNameError(t *testing.T) {
	sr := mock.NewSourceRepository()
	_, err := sr.FindByName("NotARealName")
	assert.NotNil(t, err)
}

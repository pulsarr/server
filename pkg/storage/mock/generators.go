package mock

import (
	"fmt"
	"net/url"
	"time"

	"gitlab.com/pulsarr/server/pkg/manga"
	"gitlab.com/pulsarr/server/pkg/source"
)

func NewURL() *url.URL {
	return &url.URL{
		Scheme: "http",
		Host:   "www.example.com",
	}
}

func NewSource() *source.Source {
	return &source.Source{
		Name: fmt.Sprintf("%s%s", "Name", time.Now().Format(time.RFC3339Nano)),
		URL:  NewURL(),
	}
}

func NewPage() *manga.Page {
	return &manga.Page{
		URL:         NewURL(),
		DateScraped: time.Now(),
	}
}

func NewChapter() *manga.Chapter {
	return &manga.Chapter{
		URL:          NewURL(),
		Pages:        []*manga.Page{NewPage()},
		Title:        fmt.Sprintf("%s%s", "Title", time.Now().Format(time.RFC3339Nano)),
		DateReleased: time.Now(),
		DateScraped:  time.Now(),
	}
}

func NewSeries() *manga.Series {
	return &manga.Series{
		Title:        fmt.Sprintf("%s%s", "Title", time.Now().Format(time.RFC3339Nano)),
		URL:          NewURL(),
		Source:       NewSource(),
		Chapters:     []*manga.Chapter{NewChapter()},
		Completed:    false,
		DateReleased: time.Now(),
		DateScraped:  time.Now(),
		Description:  fmt.Sprintf("%s%s", "description", time.Now().Format(time.RFC3339Nano)),
		Artists:      []string{"Artist1"},
		Authors:      []string{"Author1"},
	}
}

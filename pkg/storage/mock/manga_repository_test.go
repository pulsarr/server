package mock_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/pulsarr/server/pkg/manga"

	"gitlab.com/pulsarr/server/pkg/storage/mock"
)

func assertSeriesEqual(t *testing.T, expected, actual *manga.Series) {
	assert.Equal(t, expected.ID, actual.ID)
	assert.Equal(t, expected.Title, actual.Title)
	assert.Equal(t, expected.URL, actual.URL)
	assertSameSource(t, expected.Source, actual.Source)
	assertChapterEqual(t, expected.Chapters, actual.Chapters)
	assert.Equal(t, expected.Completed, actual.Completed)
	assert.Equal(t, expected.DateReleased, actual.DateReleased)
	assert.Equal(t, expected.DateScraped, actual.DateScraped)
	assert.Equal(t, expected.Description, actual.Description)
	assert.Equal(t, expected.Artists, actual.Artists)
	assert.Equal(t, expected.Authors, actual.Authors)
}

func assertChapterEqual(t *testing.T, expected, actual []*manga.Chapter) {
	assert.Equal(t, len(expected), len(actual))

	for i := 0; i < len(expected); i++ {
		assert.Equal(t, expected[i].URL, actual[i].URL)
		assertPageEqual(t, expected[i].Pages, actual[i].Pages)
		assert.Equal(t, expected[i].Title, actual[i].Title)
		assert.Equal(t, expected[i].DateScraped, actual[i].DateScraped)
		assert.Equal(t, expected[i].DateReleased, actual[i].DateReleased)
	}
}

func assertPageEqual(t *testing.T, expected, actual []*manga.Page) {
	assert.Equal(t, len(expected), len(actual))
	for i := 0; i < len(expected); i++ {
		assert.Equal(t, expected[i].URL, actual[i].URL)
		assert.Equal(t, expected[i].Image, actual[i].Image)
		assert.Equal(t, expected[i].DateScraped, actual[i].DateScraped)
	}
}

func TestSaveAndUpdateSeries(t *testing.T) {
	mr := mock.NewMangaRepository()

	originalSeries := mock.NewSeries()
	err := mr.Save(originalSeries)
	assert.Nil(t, err)

	allSeries, err := mr.All(nil)
	assert.Nil(t, err)
	assert.Len(t, allSeries, 1)

	firstSource := allSeries[0]
	assert.NotEqual(t, originalSeries.ID, "")
	assertSeriesEqual(t, originalSeries, firstSource)

	updatedSource := mock.NewSeries()
	updatedSource.ID = originalSeries.ID
	err = mr.Save(updatedSource)
	assert.Nil(t, err)

	firstSource = allSeries[0]
	assertSeriesEqual(t, updatedSource, firstSource)
}

func TestSeriesIDNotFound(t *testing.T) {
	mr := mock.NewMangaRepository()

	s := mock.NewSeries()
	s.ID = "NotGoingToBeFound"
	err := mr.Save(s)
	assert.NotNil(t, err)
}

func TestSeriesFind(t *testing.T) {
	mr := mock.NewMangaRepository()

	originalSeries := mock.NewSeries()
	err := mr.Save(originalSeries)
	assert.Nil(t, err)

	found, err := mr.Find(originalSeries.ID)
	assert.Nil(t, err)
	assertSeriesEqual(t, originalSeries, found)
}

func TestSeriesFindNotFound(t *testing.T) {
	mr := mock.NewMangaRepository()

	_, err := mr.Find("NotARealID")
	assert.NotNil(t, err)
}

func TestSeriesFindBySource(t *testing.T) {
	mr := mock.NewMangaRepository()

	originalSeries := mock.NewSeries()
	err := mr.Save(originalSeries)
	assert.Nil(t, err)

	found, err := mr.FindBySource(originalSeries.Source.ID)
	assert.Nil(t, err)

	assert.Len(t, found, 1)
	assertSeriesEqual(t, originalSeries, found[0])
}

func TestSeriesFindByTitle(t *testing.T) {
	mr := mock.NewMangaRepository()

	originalSeries := mock.NewSeries()
	err := mr.Save(originalSeries)
	assert.Nil(t, err)

	found, err := mr.FindByTitle(originalSeries.Title)
	assert.Nil(t, err)

	assert.Len(t, found, 1)
	assertSeriesEqual(t, originalSeries, found[0])
}

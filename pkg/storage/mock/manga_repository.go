package mock

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/pulsarr/server/pkg/manga"
)

type mangaRepository struct{}

var series []*manga.Series

func NewMangaRepository() manga.Repository {
	series = []*manga.Series{}
	return &mangaRepository{}
}

func (mr mangaRepository) Save(s *manga.Series) error {
	if s.ID == "" {
		s.ID = fmt.Sprintf(
			"%s%d%d%d%d",
			time.Now().Format(time.RFC3339),
			rand.Intn(10),
			rand.Intn(10),
			rand.Intn(10),
			rand.Intn(10),
		)
		series = append(series, s)
		return nil
	}

	for i := 0; i < len(series); i++ {
		if s.ID == series[i].ID {
			series[i] = s
			return nil
		}
	}

	return fmt.Errorf("%s%s", "Unfound series with ID", s.ID)
}

func (mr mangaRepository) All(*manga.AllOpts) ([]*manga.Series, error) {
	return series, nil
}

func (mr mangaRepository) Find(id string) (*manga.Series, error) {
	for i := 0; i < len(series); i++ {
		if id == series[i].ID {
			return series[i], nil
		}
	}

	return nil, fmt.Errorf("%s%s", "Unfound series with ID", id)
}

func (mr mangaRepository) FindByTitle(title string) ([]*manga.Series, error) {
	var foundSeries []*manga.Series

	for i := 0; i < len(series); i++ {
		if title == series[i].Title {
			foundSeries = append(foundSeries, series[i])
		}
	}

	return foundSeries, nil
}

func (mr mangaRepository) FindBySource(sourceID string) ([]*manga.Series, error) {
	var foundSeries []*manga.Series

	for i := 0; i < len(series); i++ {
		if sourceID == series[i].Source.ID {
			foundSeries = append(foundSeries, series[i])
		}
	}

	return foundSeries, nil
}

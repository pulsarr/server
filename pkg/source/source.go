package source

import (
	"net/url"
)

type (
	Source struct {
		ID   string
		Name string
		URL  *url.URL
	}

	Repository interface {
		Save(*Source) error

		All() ([]*Source, error)

		Find(id string) (*Source, error)
		FindByName(name string) (*Source, error)
	}
)

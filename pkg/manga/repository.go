package manga

type (
	Repository interface {
		Save(*Series) error

		All(*AllOpts) ([]*Series, error)

		Find(id string) (*Series, error)
		FindByTitle(title string) ([]*Series, error)
		FindBySource(sourceID string) ([]*Series, error)
	}

	AllOpts struct {
		PageSize  *int64
		PageIndex *int64
	}
)

package manga

import (
	"net/url"
	"os"
	"time"

	"gitlab.com/pulsarr/server/pkg/source"
)

type (
	// Series is Naruto, or One Piece
	Series struct {
		ID    string
		Title string
		URL   *url.URL

		Source   *source.Source
		Chapters []*Chapter

		Completed    bool
		DateReleased time.Time
		DateScraped  time.Time
		Description  string

		Artists []string
		Authors []string
	}

	// Chapter is a monthly release or volume
	Chapter struct {
		URL *url.URL

		Pages []*Page

		Title        string
		DateReleased time.Time
		DateScraped  time.Time
	}

	// Page is the literal thing you read
	Page struct {
		URL *url.URL

		DateScraped time.Time

		Image *os.File
	}
)

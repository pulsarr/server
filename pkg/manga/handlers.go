package manga

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
)

type (
	errorResponse struct {
		Error string `json:"error"`
	}

	findRequest struct {
		ID       string `json:"id"`
		Title    string `json:"title"`
		SourceID string `json:"source_id"`
		Limit    int    `json:"limit"`
	}

	seriesResponse struct {
		ID           string             `json:"id,omitempty"`
		SourceID     string             `json:"source_id,omitempty"`
		Title        string             `json:"title,omitempty"`
		URL          string             `json:"url,omitempty"`
		Chapters     []*chapterResponse `json:"chapters,omitempty"`
		Artists      []string           `json:"artists,omitempty"`
		Authors      []string           `json:"authors,omitempty"`
		Completed    bool               `json:"completed,omitempty"`
		DateReleased string             `json:"date_released,omitempty"`
		DateScraped  string             `json:"date_scraped,omitempty"`
		Description  string             `json:"description,omitempty"`
	}

	chapterResponse struct {
		URL          string          `json:"url,omitempty"`
		Pages        []*pageResponse `json:"pages,omitempty"`
		Title        string          `json:"title,omitempty"`
		DateReleased string          `json:"date_released,omitempty"`
		DateScraped  string          `json:"date_scraped,omitempty"`
	}

	pageResponse struct {
		URL         string `json:"url,omitempty"`
		DateScraped string `json:"date_scraped,omitempty"`
		ImageURL    string `json:"image_url,omitempty"`
	}
)

func (s *Series) response() *seriesResponse {
	return &seriesResponse{
		ID:           s.ID,
		SourceID:     s.Source.ID,
		Title:        s.Title,
		URL:          s.URL.String(),
		Chapters:     convertChapters(s.Chapters),
		Artists:      s.Artists,
		Authors:      s.Authors,
		Completed:    s.Completed,
		DateReleased: s.DateReleased.Format(time.RFC3339),
		DateScraped:  s.DateScraped.Format(time.RFC3339),
		Description:  s.Description,
	}
}

func convertChapters(chapters []*Chapter) []*chapterResponse {
	var chapterResponses []*chapterResponse

	for i := 0; i < len(chapters); i++ {
		chapterResponses = append(chapterResponses, chapters[i].response())
	}

	return chapterResponses
}

func (c *Chapter) response() *chapterResponse {
	return &chapterResponse{
		URL:          c.URL.String(),
		Pages:        convertPages(c.Pages),
		Title:        c.Title,
		DateReleased: c.DateReleased.Format(time.RFC3339),
		DateScraped:  c.DateReleased.Format(time.RFC3339),
	}
}

func convertPages(pages []*Page) []*pageResponse {
	var pageResponses []*pageResponse

	for i := 0; i < len(pages); i++ {
		pageResponses = append(pageResponses, pages[i].response())
	}

	return pageResponses
}

func (p *Page) response() *pageResponse {
	// TODO: Fix actual path to server based url
	imageURL := ""
	if p.Image != nil {
		imageURL = p.Image.Name()
	}

	return &pageResponse{
		URL:         p.URL.String(),
		DateScraped: p.DateScraped.Format(time.RFC3339),
		ImageURL:    imageURL,
	}
}

func findResponse(c echo.Context, series []*Series) error {
	responses := []seriesResponse{}

	for i := 0; i < len(series); i++ {
		fs := series[i]
		responses = append(responses, *fs.response())
	}

	return c.JSON(http.StatusOK, responses)
}

func (s *service) FindHandler(c echo.Context) error {
	findRequest := new(findRequest)

	if err := c.Bind(findRequest); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, "Please check payload")
	}

	if err := c.Validate(findRequest); err != nil {
		trans, _ := s.uni.GetTranslator("en")

		for _, e := range err.(validator.ValidationErrors) {
			return c.JSON(http.StatusUnprocessableEntity, errorResponse{Error: e.Translate(trans)})
		}
	}

	if findRequest.ID != "" {
		series, err := s.mr.Find(findRequest.ID)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, errorResponse{
				Error: fmt.Sprintf("%s%s", "Unable to get Series by id: ", findRequest.ID),
			})
		}

		return findResponse(c, []*Series{series})
	}

	if findRequest.Title != "" {
		series, err := s.mr.FindByTitle(findRequest.Title)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, errorResponse{
				Error: fmt.Sprintf("%s%s", "Unable to get Series by title: ", findRequest.Title),
			})
		}
		return findResponse(c, series)
	}

	if findRequest.SourceID != "" {
		series, err := s.mr.FindBySource(findRequest.SourceID)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, errorResponse{
				Error: fmt.Sprintf("%s%s", "Unable to get Series by source_id: ", findRequest.SourceID),
			})
		}
		return findResponse(c, series)
	}

	series, err := s.mr.All(nil)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, errorResponse{
			Error: "Unable to get all Series",
		})
	}
	return findResponse(c, series)
}

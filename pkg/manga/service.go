package manga

import (
	"github.com/labstack/echo/v4"

	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	en_translations "github.com/go-playground/validator/v10/translations/en"
)

type (
	Service interface {
		FindHandler(c echo.Context) error
	}

	service struct {
		mr        Repository
		uni       *ut.UniversalTranslator
		validator *validator.Validate
	}
)

func (s *service) Validate(i interface{}) error {
	return s.validator.Struct(i)
}

func NewService(mr Repository, e *echo.Echo) Service {
	s := &service{
		mr: mr,
	}

	enLoc := en.New()
	s.uni = ut.New(enLoc, enLoc)
	s.validator = validator.New()

	e.Validator = s

	trans, _ := s.uni.GetTranslator("en")

	_ = en_translations.RegisterDefaultTranslations(s.validator, trans)

	return s
}

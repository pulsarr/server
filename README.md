# Pulsarr Server
Backend responsible for maintaining a users manga collection

## Status
[![coverage report](https://gitlab.com/pulsarr/server/badges/badges/coverage.svg)](https://gitlab.com/pulsarr/server/-/commits/badges)

## Project Dependencies

* [Go](https://golang.org/doc/install)

## Getting Started

Run `make` which will list all make targets

### Service Environment Variables
`PULSARR_DOWNLOADS` // Place to save manga images

## Development
### Vs Code
In order to debug integration tests add this to your workspace config for this workspace `"go.buildFlags": ["-tags=integration"]`

## Architecture

* Philosophy: https://medium.com/@benbjohnson/standard-package-layout-7cdbc8391fc1
* Domain Example: https://github.com/katzien/go-structure-examples/tree/master/domain

## Words and Naming things

Words are hard please check out [Words](./words.md) as this project is following DDD.

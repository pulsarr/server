package main

import (
	"fmt"

	"gitlab.com/pulsarr/server/pkg/scraper"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/pulsarr/server/pkg/manga"
	"gitlab.com/pulsarr/server/pkg/storage/mongo"
)

const (
	port = 1323
)

func main() {
	e := echo.New()

	storage, _ := mongo.New()
	mangaRepository := storage.NewMangaRepository()
	sourceRepository := storage.NewSourceRepository()

	mangaService := manga.NewService(mangaRepository, e)

	// TODO: https://gitlab.com/pulsarr/server/issues/13
	scraperService := scraper.New(
		sourceRepository,
		mangaRepository,
		scraper.MangaReader,
	)

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Use(middleware.StaticWithConfig(middleware.StaticConfig{
		Root:   "../../temp",
		Browse: true,
	}))

	e.POST("/mangas", mangaService.FindHandler)

	e.POST("/scrape", scraperService.ScrapeAllSeriesHandler)
	e.POST("/scrape/:series_id", scraperService.ScrapeAllChaptersHandler)
	e.POST("/scrape/:series_id/all", scraperService.ScrapeAllPagesHandler)

	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", port)))
}

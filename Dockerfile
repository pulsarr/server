# Foundation for all builds / ci
FROM golang:1.14 AS build

RUN curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.27.0
RUN go get -u \
    github.com/securego/gosec/cmd/gosec

WORKDIR /app
COPY go.* ./
COPY Makefile ./

RUN make deps

COPY . ./

RUN make build

# Prod ready runable image/container 
FROM ubuntu:disco AS app
COPY --from=build  /app/bin/pulsarr-server /app/
EXPOSE 8080
ENTRYPOINT [ "/app/pulsarr-server" ]

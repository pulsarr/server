# Core Domain Manga

## Source

The place where a given Series comes from

## Series

The thing most people call manga. One piece, Naruto, etc. The literal Manga Series.

## Chapter

This is both an individual chapter 101, but also can be volumes V1 (C01-C10). Since we can't know before hand what each source is doing.

## Page

The literal page a person reads. Part meta data, but the core is the image itself.

# Scraper Domain


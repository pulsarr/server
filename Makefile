.PHONY: list
list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

deps:
	go mod tidy

build: clean deps
	env go build -o bin/pulsarr-server cmd/pulsarr-server/main.go

server: clean deps
	env go run cmd/pulsarr-server/main.go

clean:
	rm -rf ./bin ./vendor Gopkg.lock

lint: deps
	golangci-lint run -c ./.golangci.yml

test: deps
	go test -cover ./...

inttest: deps
	go test -cover -tags=integration ./...

security: deps
	gosec -tests ./...
